# LearnIt - Project Mini
***
## Backend
1. Technology used
- Nodejs, Express
- jsonwebtoken (authenticate)
- argon2 (hash password)
- MongoDB (server), mongoose (server connect with database)
- cors (help client call api to server)
2. Setup
- config mongodb
- npm install
- npm run server

## Frontend
1. Technology used
- Reactjs
- Context/Redux
- react-router-dom
- react-bootstrap
2. Setup
- npm install 
- npm start