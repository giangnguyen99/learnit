import React, { useContext, useState } from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import { Link } from 'react-router-dom'
import { AuthContext } from '../../context/AuthContext'
import AlertMessage from '../layout/AlertMessage'
const RegisterForm = () => {

  // Context
  const { registerUser } = useContext(AuthContext)



  // Local state
  const [registerForm, setRegisterForm] = useState({
    email: '',
    username: '',
    password: '',
    confirmPassword: ''
  })
  const { email, username, password, confirmPassword } = registerForm
  const [alert, setAlert] = useState(null)
  const onChangeRegisterForm = (e) => {
    const { name, value } = e.target
    setRegisterForm({
      ...registerForm,
      [name]: value
    })
  }

  const register = async e => {
    e.preventDefault()
    if (password !== confirmPassword) {
      setAlert({ type: 'danger', message: 'Password do not match' })
      setTimeout(() => {
        setAlert(null)
      }, 5000);
      return
    }


    try {
      const registerData = await registerUser(registerForm)
      if (!registerData.success) {
        setAlert({ type: 'Danger', message: registerData.message })
        setTimeout(() => setAlert(null), 5000)
      }
    } catch (error) {
      console.log(error)
    }

  }


  return (
    <>
      <Form className='my-4' onSubmit={register}>
        <AlertMessage info={alert} />
        <Form.Group>
          <Form.Control type='text' placeholder='Email' name='email' required value={email} onChange={onChangeRegisterForm} />
        </Form.Group>
        <Form.Group>
          <Form.Control type='text' placeholder='Username' name='username' required value={username} onChange={onChangeRegisterForm} />
        </Form.Group>
        <Form.Group>
          <Form.Control type='password' placeholder='Password' name='password' required value={password} onChange={onChangeRegisterForm} />
        </Form.Group>
        <Form.Group>
          <Form.Control type='password' placeholder='Confirm Password' name='confirmPassword' required value={confirmPassword} onChange={onChangeRegisterForm} />
        </Form.Group>
        <Button variant='success' type='submit'>Register</Button>
      </Form>
      <p> Already have an account?
        <Link to='/login'>
          <Button variant='info' size='sm' className='ml-2'>Login</Button>
        </Link>
      </p>
    </>
  )
}

export default RegisterForm
