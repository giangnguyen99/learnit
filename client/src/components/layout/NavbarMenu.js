import React, { useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import learnItLogo from '../../assets/logo.svg'
import { Link } from 'react-router-dom'
import { AuthContext } from '../../context/AuthContext'
import NavDropdown from 'react-bootstrap/NavDropdown'

const NavbarMenu = () => {

  const { authState: { user: { username, avatar } }, logoutUser } = useContext(AuthContext)

  const logout = () => logoutUser()
  return (
    <Navbar expand='lg' bg='primary' variant='dark' className='shadow'>
      <div className='container'>
        <Navbar.Brand className='font-weight-bolder text-white'>
          <img src={learnItLogo} alt='learnitLogo' width='32' height='32' className='mr-2' />
          LearnIt
        </Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
        <Navbar.Collapse id='basic-navbar-nav'>
          <Nav className='mr-auto'>
            <Nav.Link className='font-weight-bolder text-white' to='/dashboard' as={Link}>
              Dashboard
            </Nav.Link>
          </Nav>

          <Nav>
            <Nav.Link className='font-weight-bolder text-white welcome' disabled>
              Welcome {username}
            </Nav.Link>
            <NavDropdown title={<>
              <img src={avatar} alt='avatar' width='40' height='40' style={{ borderRadius: '50%' }} className='mr-5' />
            </>} id="nav-dropdown" >
              <NavDropdown.Item href="/user">Profile</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item onClick={logout}>Log out</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </div>
    </Navbar >
  )
}

export default NavbarMenu
