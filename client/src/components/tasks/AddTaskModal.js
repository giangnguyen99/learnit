import React, { useContext, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { TaskContext } from '../../context/TaskContext'
const AddTaskModal = () => {
  const { setShowToast, showAddTaskModal, setShowAddTaskModal, addTask } = useContext(TaskContext)

  const [newTask, setNewTask] = useState({
    title: '',
    description: '',
    url: '',
    status: 'TO LEARN'
  })

  const { title, description, url } = newTask

  const onChangeNewTaskForm = e => {
    const { name, value } = e.target
    setNewTask({
      ...newTask,
      [name]: value
    })
  }

  const onSubmitForm = async e => {
    e.preventDefault()
    const { success, message } = await addTask(newTask)
    resetAddTaskData()
    setShowToast({
      show: success,
      message,
      type: success ? 'success' : 'danger'
    })
  }

  const resetAddTaskData = () => {
    setNewTask({
      title: '',
      description: '',
      url: '',
      status: 'TO LEARN'
    })
    setShowAddTaskModal(false)
  }



  return (
    <Modal show={showAddTaskModal} onHide={resetAddTaskData}>
      <Modal.Header closeButton>
        <Modal.Title>What do you want to learn?</Modal.Title>
      </Modal.Header>
      <Form onSubmit={onSubmitForm}>
        <Modal.Body>
          <Form.Group>
            <Form.Control type='text' placeholder='Title' name='title' required aria-describedby='title-help' value={title} onChange={onChangeNewTaskForm} />
            <Form.Text id='title-help' muted>Requied</Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Control as='textarea' rows={3} placeholder='Description' name='description' value={description} onChange={onChangeNewTaskForm} />
          </Form.Group>
          <Form.Group>
            <Form.Control type='text' placeholder='Youtube Tutorial URL' name='url' value={url} onChange={onChangeNewTaskForm} />
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={resetAddTaskData}>Cancel</Button>
          <Button variant='primary' type='submit'>LearnIt!</Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default AddTaskModal
