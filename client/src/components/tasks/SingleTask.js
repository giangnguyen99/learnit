import React from 'react'
import { Badge, Card, Col, Row } from 'react-bootstrap'
import ActionButton from './ActionButton'

const SingleTask = ({ task: { _id, status, title, description, url } }) => {



  return (
    <Card className='shadow' border={status === 'LEARNED' ? 'success' : status === 'LEARNING' ? 'warning' : 'danger'}>
      <Card.Body>
        <Card.Title>
          <Row>
            <Col xs={8}>
              <p className='task-title'>{title.length > 40 ? `${title.substring(0, 39)}...` : title}</p>
              <Badge pill variant={status === 'LEARNED' ? 'success' : status === 'LEARNING' ? 'warning' : 'danger'}>
                {status}
              </Badge>
            </Col>
            <Col className='act-btn'>
              <ActionButton url={url} _id={_id} />
            </Col>
          </Row>
        </Card.Title>
        <Card.Text>{description.length > 70 ? `${description.substring(0, 39)}...` : description}</Card.Text>
      </Card.Body>
    </Card>
  )
}

export default SingleTask
