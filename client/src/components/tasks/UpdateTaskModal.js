import React, { useContext, useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { TaskContext } from '../../context/TaskContext'
const UpdateTaskModal = () => {
  const { taskState: { task }, setShowToast, showUpdateTaskModal, setShowUpdateTaskModal, updateTask } = useContext(TaskContext)

  const [updatedTask, setUpdatedTask] = useState(task)

  useEffect(() => {
    setUpdatedTask(task)
  }, [task])
  const { title, description, url, status } = updatedTask

  const onChangeUpdatedTaskForm = e => {
    const { name, value } = e.target
    setUpdatedTask({
      ...updatedTask,
      [name]: value
    })
  }

  const onSubmitForm = async e => {
    e.preventDefault()
    const { success, message } = await updateTask(updatedTask)
    setShowUpdateTaskModal(false)

    setShowToast({
      show: success,
      message,
      type: success ? 'success' : 'danger'
    })
  }

  const closeDialog = () => {
    setUpdatedTask(task)
    setShowUpdateTaskModal(false)
  }
  // const resetAddTaskData = () => {
  //   setNewTask({
  //     title: '',
  //     description: '',
  //     url: '',
  //     status: 'TO LEARN'
  //   })
  //   setShowAddTaskModal(false)
  // }



  return (
    <Modal show={showUpdateTaskModal} onHide={closeDialog} >
      <Modal.Header closeButton>
        <Modal.Title>Making progress?</Modal.Title>
      </Modal.Header>
      <Form onSubmit={onSubmitForm}>
        <Modal.Body>
          <Form.Group>
            <Form.Control type='text' placeholder='Title' name='title' required aria-describedby='title-help' value={title} onChange={onChangeUpdatedTaskForm} />
            <Form.Text id='title-help' muted>Requied</Form.Text>
          </Form.Group>
          <Form.Group>
            <Form.Control as='textarea' rows={3} placeholder='Description' name='description' value={description} onChange={onChangeUpdatedTaskForm} />
          </Form.Group>
          <Form.Group>
            <Form.Control type='text' placeholder='Youtube Tutorial URL' name='url' value={url} onChange={onChangeUpdatedTaskForm} />
          </Form.Group>
          <Form.Group>
            <Form.Control as='select' value={status} name='status' onChange={onChangeUpdatedTaskForm}>
              <option value='TO LEARN'>TO LEARN</option>
              <option value='LEARNING'>LEARNING</option>
              <option value='LEARNED'>LEARNED</option>
            </Form.Control>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='secondary' onClick={closeDialog}>Cancel</Button>
          <Button variant='primary' type='submit'>LearnIt!</Button>
        </Modal.Footer>
      </Form>
    </Modal>
  )
}

export default UpdateTaskModal
