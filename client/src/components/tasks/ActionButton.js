import React, { useContext } from 'react'
import { Button } from 'react-bootstrap'
import playIcon from '../../assets/play-btn.svg'
import ediitIcon from '../../assets/pencil.svg'
import deleteIcon from '../../assets/trash.svg'
import { TaskContext } from '../../context/TaskContext'



const ActionButton = ({ url, _id }) => {

  const { deleteTask, findTask, setShowUpdateTaskModal } = useContext(TaskContext)


  const chooseTask = taskId => {
    findTask(taskId)
    setShowUpdateTaskModal(true)
  }

  return (
    <>
      <Button className='task-button' href={url} target='_blank'>
        <img src={playIcon} alt='play' width='28' height='28' />
      </Button>
      <Button className='task-button' onClick={chooseTask.bind(this, _id)} >
        <img src={ediitIcon} alt='edit' width='24' height='24' />
      </Button>
      <Button className='task-button lastBtn' onClick={deleteTask.bind(this, _id)}>
        <img src={deleteIcon} alt='delete' width='24' height='24' />
      </Button>
    </>
  )
}

export default ActionButton
