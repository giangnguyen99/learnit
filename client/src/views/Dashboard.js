import { useContext, useEffect, useState } from 'react'
import { Spinner, Card, Button, Col, Row, OverlayTrigger, Tooltip, Toast } from 'react-bootstrap'
import { AuthContext } from '../context/AuthContext'
import AddTaskModal from '../components/tasks/AddTaskModal'
import addIcon from '../assets/plus-circle-fill.svg'
import calendarIcon from '../assets/calendar.svg'
import { TaskContext } from '../context/TaskContext'
import SingleTask from '../components/tasks/SingleTask'
import UpdateTaskModal from '../components/tasks/UpdateTaskModal'
import Calendar from 'react-calendar';
const Dashboard = () => {
  const [calendar, setCalendar] = useState(false)
  const [value, onChange] = useState(null);
  const changeDay = (value) => {
    onChange(value)
    setCalendar(false)
  }
  // Context
  const { authState: { user: { username } } } = useContext(AuthContext)
  const { taskState: { task, tasks, tasksLoading }, getTasks, setShowAddTaskModal, showToast: { show, message, type }, setShowToast, } = useContext(TaskContext)

  // Start: Get all tasks
  useEffect(() => {
    getTasks()
  }, [])
  let body = null

  if (tasksLoading) {
    body = (
      <div className='spinner-container'>
        <Spinner animation='border' variant='info' />
      </div>
    )
  } else if (tasks.length === 0) {
    body = (
      <>

        <Card className='text-center mx-5 my-5' >
          <Card.Header as='h1' >Hi {username}</Card.Header>
          <Card.Body>
            <Card.Title>Welcome to LearnIt</Card.Title>
            <Card.Text>
              Click the button below to track your first skill to learn
            </Card.Text>
            <Button variant='primary' onClick={setShowAddTaskModal.bind(this, true)}>LearnIt!</Button>
          </Card.Body>
        </Card>
      </>
    )
  } else {
    body = (
      <>
        {calendar &&
          <div className='lich'>
            <div className='lich1'>
              <h5 className="text-center">Calendar</h5>
              <hr />
              <Calendar onChange={changeDay}
                value={value} />
              <div className="close" onClick={() => {
                setCalendar(false)
                onChange(null)
              }}>
                &times;
              </div>
            </div>

          </div>
        }
        <Row className='row-cols-1 row-cols-md-3 g-4 mx-auto mt-3' >
          {value !== null ? (
            tasks.filter(task => {
              const convTime = (new Date(task.createdAt)).toDateString()
              return convTime === value.toDateString()
            }).map(task =>
            (
              < Col key={task._id} className='my-2' >
                <SingleTask task={task} />
              </Col>
            ))) :
            tasks.map(task =>
            (
              < Col key={task._id} className='my-2' >
                <SingleTask task={task} />
              </Col>

            ))}
        </Row>

        {/* Open Add Task Modal */}
        <OverlayTrigger placement='left' overlay={<Tooltip>Add a new thing to learn</Tooltip>}>
          <Button className='btn-floating btn-mb' onClick={setShowAddTaskModal.bind(this, true)}>
            <img src={addIcon} alt='add task' />
          </Button>
        </OverlayTrigger>
        <OverlayTrigger placement='left' overlay={<Tooltip>Display Calendar</Tooltip>}>
          <Button className='btn1-floating btn-mb' onClick={setCalendar.bind(this, true)}>
            <img src={calendarIcon} alt='calendar' />
          </Button>
        </OverlayTrigger>
      </>
    )
  }

  return <>
    {body}
    {task !== null && <UpdateTaskModal />}
    <AddTaskModal />

    {/* after task is added show toast  */}

    <Toast show={show} style={{ position: 'fixed', top: '20%', right: '10px' }} className={`bg-${type} text-white`} onClose={setShowToast.bind(this, { show: false, message: '', type: null })} delay={3000} autohide >
      <Toast.Body>
        <strong>{message}</strong>
      </Toast.Body>
    </Toast>

  </>
}

export default Dashboard
