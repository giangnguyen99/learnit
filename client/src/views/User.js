import React from 'react'
import { useState } from 'react'
import { useContext } from 'react'
import { AuthContext } from '../context/AuthContext'
import { Toast } from 'react-bootstrap'
import { TaskContext } from '../context/TaskContext'

const User = () => {
  const { authState: { user }, editUser } = useContext(AuthContext)
  const { showToast: { show, message, type }, setShowToast } = useContext(TaskContext)
  const [editForm, setEditForm] = useState({
    username: user.username,
    curPassword: '',
    newPassword: ''
  })
  const [confirmPassword, setConfirmPassword] = useState('')
  const [avatar, setAvatar] = useState('')
  const onChangeEditForm = (e) => {
    const { name, value } = e.target
    setEditForm({
      ...editForm,
      [name]: value
    })
  }
  const onChangeConfirmPassword = (e) => {
    const { value } = e.target
    setConfirmPassword(value)
  }
  const changeAvatar = (e) => {
    const file = e.target.files[0]
    setAvatar(file)
  }
  const resetUser = () => {
    setEditForm({
      username: user.username,
      curPassword: '',
      newPassword: ''
    })
    setAvatar('')
    setConfirmPassword('')
  }
  const handleSubmit = async e => {
    e.preventDefault()
    if (editForm.newPassword && editForm.newPassword !== confirmPassword) {
      console.log('new: ' + editForm.newPassword)
      console.log('conf: ' + confirmPassword)
      setShowToast({
        show: true,
        message: 'Confirm password is not match',
        type: 'danger'
      })
      return;
    }

    const response = await editUser({ ...editForm, avatar })
    const { success, message } = response
    setShowToast({
      show: true,
      message,
      type: success ? 'success' : 'danger'
    })
  }
  return (
    <>
      <Toast show={show} style={{ position: 'fixed', bottom: '20%', right: '10px' }} className={`bg-${type} text-white`} onClose={setShowToast.bind(this, { show: false, message: '', type: null })} delay={3000} autohide >
        <Toast.Body>
          <strong>{message}</strong>
        </Toast.Body>
      </Toast>

      <div class="container">
        <h1 class="text-primary edit-profile"><span class="glyphicon glyphicon-user"></span>Edit Profile</h1>
        <hr />
        <div class="row">
          <div class="col-12">
            <form class="form-horizontal" onSubmit={handleSubmit}>

              <div class="panel panel-default">
                <div class="panel-body text-center info_avatar">
                  <img src={avatar ? URL.createObjectURL(avatar) : user.avatar} class="img-circle profile-avatar" alt="User avatar" />
                  <span>
                    <i className="fas fa-camera" />
                    <p>Change</p>
                    <input type="file" name="file" id="file_up"
                      accept="image/*" onChange={changeAvatar} class="form-control" />
                  </span>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">User info</h4>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input disabled type="text" class="form-control" name='email' value={user.email} onChange={onChangeEditForm} />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name='username' value={editForm.username} onChange={onChangeEditForm} />
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">Security</h4>
                </div>
                <div class="panel-body">
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Current password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name='curPassword' value={editForm.curPassword} onChange={onChangeEditForm} />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">New password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name='newPassword' value={editForm.newPassword} onChange={onChangeEditForm} />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Confirm password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name='confirmPassword' value={confirmPassword} onChange={onChangeConfirmPassword} />
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                      <button type="submit" class="btn btn-primary save">Save</button>
                      <button type='button' class="btn btn-danger" onClick={resetUser} >Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div >
    </>
  )
}

export default User