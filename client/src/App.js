import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Landing from './components/layout/Landing';
import Auth from './views/Auth';
import AuthContextProvider from './context/AuthContext';
import ProtectedRoute from './components/routing/ProtectedRoute';
import Dashboard from './views/Dashboard';
import User from './views/User';
import TaskContextProvider from './context/TaskContext';
import 'react-calendar/dist/Calendar.css';
function App() {
  return (
    <AuthContextProvider>
      <TaskContextProvider>
        <Router>
          <Switch>
            <Route exact path='/' component={Landing} />
            <Route exact path='/login' render={props => <Auth {...props} authRoute='login' />} />
            <Route exact path='/register' render={props => <Auth {...props} authRoute='register' />} />

            <ProtectedRoute exact path='/dashboard' component={Dashboard} />

            <ProtectedRoute exact path='/user' component={User} />
            <Route component={NotFound} />

          </Switch>
        </Router>
      </TaskContextProvider>
    </AuthContextProvider>
  );
}

const NotFound = () => {
  return (
    <div className="position-relative" style={{ minHeight: 'calc(100vh - 70px)' }}>
      <h2 className="position-absolute text-secondary"
        style={{ top: '50%', left: '50%', transform: 'translate(-50%, -50%)' }}>
        404 | Not Found.
      </h2>
    </div>
  )
}
export default App;
