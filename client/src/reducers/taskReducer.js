import { ADD_TASK, DELETE_TASK, FIND_TASK, TASKS_LOADED_FAIL, TASKS_LOADED_SUCCESS, UPDATE_TASK } from "../context/constants"

export const taskReducer = (state, action) => {

  const { type, payload } = action
  switch (type) {
    case TASKS_LOADED_SUCCESS:
      return {
        ...state,
        tasks: payload,
        tasksLoading: false
      }
    case TASKS_LOADED_FAIL:
      return {
        ...state,
        tasks: [],
        tasksLoading: false
      }
    case ADD_TASK:
      return {
        ...state,
        tasks: [
          ...state.tasks,
          payload
        ]
      }
    case DELETE_TASK:
      return {
        ...state,
        tasks: state.tasks.filter(task => task._id !== payload)
      }
    case FIND_TASK:
      return {
        ...state,
        task: payload
      }
    case UPDATE_TASK:
      const newTasks = state.tasks.map(task => task._id === payload._id ? payload : task)
      return {
        ...state,
        tasks: newTasks
      }
    default:
      return state
  }
}