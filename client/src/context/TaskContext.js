import { createContext, useReducer, useState } from "react";
import { taskReducer } from '../reducers/taskReducer'
import axios from 'axios'
import { ADD_TASK, apiUrl, DELETE_TASK, FIND_TASK, TASKS_LOADED_FAIL, TASKS_LOADED_SUCCESS, UPDATE_TASK } from './constants'


export const TaskContext = createContext()

const TaskContextProvider = ({ children }) => {

  const [taskState, dispatch] = useReducer(taskReducer, {
    task: null,
    taks: [],
    tasksLoading: true
  })

  const [showAddTaskModal, setShowAddTaskModal] = useState(false)
  const [showUpdateTaskModal, setShowUpdateTaskModal] = useState(false)
  const [showToast, setShowToast] = useState({
    show: false,
    message: '',
    type: null
  })

  // get all tasks
  const getTasks = async () => {
    try {
      const response = await axios.get(`${apiUrl}/tasks`)
      if (response.data.success) {
        dispatch({ type: TASKS_LOADED_SUCCESS, payload: response.data.tasks })
      }

    } catch (error) {
      dispatch({ type: TASKS_LOADED_FAIL })
    }
  }

  // add new task
  const addTask = async newTask => {
    try {
      const response = await axios.post(`${apiUrl}/tasks`, newTask)
      if (response.data.success) {
        dispatch({ type: ADD_TASK, payload: response.data.task })
        return response.data
      }
    } catch (error) {
      return error.response.data ? error.response.data : { success: false, message: 'server error' }
    }
  }


  // delete task
  const deleteTask = async taskId => {
    try {
      const response = await axios.delete(`${apiUrl}/tasks/${taskId}`)
      if (response.data.success) {
        dispatch({ type: DELETE_TASK, payload: taskId })
      }
    } catch (error) {
      console.log(error)
    }
  }

  // find task when user is updating task 

  const findTask = taskId => {
    const task = taskState.tasks.find(task => task._id === taskId)
    dispatch({ type: FIND_TASK, payload: task })
  }


  // update task
  const updateTask = async updatedTask => {
    try {
      const response = await axios.put(`${apiUrl}/tasks/${updatedTask._id}`, updatedTask)
      if (response.data.success) {
        dispatch({ type: UPDATE_TASK, payload: response.data.task })
        return response.data
      }


    } catch (error) {
      return error.response.data ? error.response.data : { success: false, message: 'server error' }
    }
  }

  const taskContextData = { showUpdateTaskModal, setShowUpdateTaskModal, findTask, updateTask, taskState, getTasks, addTask, showAddTaskModal, setShowAddTaskModal, showToast, setShowToast, deleteTask }

  return (
    <TaskContext.Provider value={taskContextData}>
      {children}
    </TaskContext.Provider>
  )
}

export default TaskContextProvider