import { createContext, useEffect, useReducer } from "react";
import axios from 'axios'
import { authReducer } from "../reducers/authReducer";
import { apiUrl, LOCAL_STORAGE_TOKEN_NAME, SET_AUTH } from './constants'
import setAuthToken from '../utils/setAuthToken'
import { imageUpload } from '../utils/imageUpload'

export const AuthContext = createContext()

const AuthContextProvider = ({ children }) => {
  const [authState, dispatch] = useReducer(authReducer, {
    authLoading: true,
    isAuthenticated: false,
    user: null
  })

  // Authenticate user
  const loadUser = async () => {
    if (localStorage[LOCAL_STORAGE_TOKEN_NAME]) {
      setAuthToken(localStorage[LOCAL_STORAGE_TOKEN_NAME])
    }

    try {
      const response = await axios.get(`${apiUrl}/auth`)
      if (response.data.success) {
        dispatch({ type: SET_AUTH, payload: { isAuthenticated: true, user: response.data.user } })
      }
    } catch (error) {
      localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
      setAuthToken(null)
      dispatch({ type: SET_AUTH, payload: { isAuthenticated: false, user: null } })
    }
  }
  useEffect(() => {
    loadUser()
  }, [])


  // Login
  const loginUser = async userForm => {
    try {
      const response = await axios.post(`${apiUrl}/auth/login`, userForm)
      if (response.data.success)
        localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, response.data.accessToken)

      loadUser()
      return response.data

    } catch (error) {
      if (error.response.data) return error.response.data
      else return { success: false, message: error.message }
    }
  }

  // Register
  const registerUser = async userForm => {
    try {
      const response = await axios.post(`${apiUrl}/auth/register`, userForm)
      if (response.data.success)
        localStorage.setItem(LOCAL_STORAGE_TOKEN_NAME, response.data.accessToken)

      loadUser()
      return response.data

    } catch (error) {
      if (error.response.data) return error.response.data
      else return { success: false, message: error.message }
    }
  }

  // Log out
  const logoutUser = () => {
    localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME)
    dispatch({ type: SET_AUTH, payload: { isAuthenticated: false, user: null } })
  }


  // Edit user 
  const editUser = async (userForm) => {
    const { username, curPassword, newPassword, avatar } = userForm
    try {
      let media
      if (avatar) media = await imageUpload([avatar])

      const response = await axios.put(`${apiUrl}/user`, { username, curPassword, newPassword, avatar: avatar ? media[0].url : authState.user.avatar })
      if (response.data.success) {
        loadUser()
      }
      return response.data
    } catch (error) {
      if (error.response.data) return error.response.data
      else return { success: false, message: error.message }
    }
  }



  // Context Data
  const authContextData = { loginUser, registerUser, logoutUser, editUser, authState }

  // Return provider
  return (
    <AuthContext.Provider value={authContextData}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthContextProvider