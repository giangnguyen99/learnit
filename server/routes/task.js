const express = require('express')
const router = express.Router()

const verifyToken = require('../middlewares/auth')
const Task = require('../models/Task')


// @route GET api/tasks
// @desc GET tasks
// @access Private
router.get('/', verifyToken, async (req, res) => {
  try {
    const tasks = await Task.find({ user: req.userId }).populate('user', ['username'])
    res.json({ success: true, tasks })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})



// @route POST api/tasks
// @desc Create task
// @access Private

router.post('/', verifyToken, async (req, res) => {
  const { title, description, url, status } = req.body

  //Simple validation
  if (!title)
    return res.status(400).json({ success: false, message: 'Title is required' })

  try {
    const newTask = new Task({
      title,
      description,
      url: (url.startsWith('https://')) ? url : `https://${url}`,
      status: status || 'TO LEARN',
      user: req.userId
    })
    await newTask.save()

    res.json({ success: true, message: 'Happy learning!', task: newTask })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})


// @route PUT api/tasks
// @desc Update task
// @access Private
router.put('/:id', verifyToken, async (req, res) => {
  const { title, description, url, status } = req.body

  //Simple validation
  if (!title)
    return res.status(400).json({ success: false, message: 'Title is required' })

  try {
    let updatedTask = {
      title,
      description: description || '',
      url: (url.startsWith('https://') ? url : `http://${url}`) || '',
      status: status || 'TO LEARN'
    }
    const taskUpdateCondition = { _id: req.params.id, user: req.userId }

    updatedTask = await Task.findOneAndUpdate(taskUpdateCondition, updatedTask, { new: true })
    //User not authorised to update task or task not found
    if (!updatedTask)
      return res.status(401).json({ success: false, message: 'Post not found or user not authorised' })

    res.json({ success: true, message: 'Excellent progress!', task: updatedTask })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})

// @route DELTE api/tasks
// @desc Delete task
// @access Private

router.delete('/:id', verifyToken, async (req, res) => {
  try {
    const taskDeleteCondition = { _id: req.params.id, user: req.userId }

    const deletedTask = await Task.findOneAndDelete(taskDeleteCondition)

    // User not authorised or task not found
    if (!deletedTask)
      return res.status(401).json({ success: false, message: 'Post not found or user not authorised' })

    res.json({ success: true, task: deletedTask })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})

module.exports = router