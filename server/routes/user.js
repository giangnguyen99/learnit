const express = require('express')
const router = express.Router()
const argon2 = require('argon2')
const verifyToken = require('../middlewares/auth')


const User = require('../models/User')


router.put('/', verifyToken, async (req, res) => {
  try {

    const { avatar, username, curPassword, newPassword } = req.body
    console.log(req.body.avatar)
    const user = await User.findOne({ _id: req.userId })
    if (!curPassword)
      return res.status(400).json({ success: false, message: 'Please add your current password' })
    const curPasswordValid = await argon2.verify(user.password, curPassword)
    if (!curPasswordValid)
      return res.status(400).json({ success: false, message: 'Current password is not match' })

    if (username !== user.username) {
      const username_exist = await User.findOne({ username })
      if (username_exist) {
        return res.status(400).json({ success: false, message: 'Username is exist, please try other' })
      }
    }
    if (newPassword) {
      const hashedPassword = await argon2.hash(newPassword)
      await User.findOneAndUpdate({ _id: req.userId }, {
        avatar, username, password: hashedPassword
      }, { new: true })
    } else {
      await User.findOneAndUpdate({ _id: req.userId }, {
        avatar, username
      }, { new: true })
    }
    res.json({ success: true, message: "Update Success!" })

  } catch (err) {
    return res.status(500).json({ success: false, mesage: 'Internal server error' })
  }
})

module.exports = router