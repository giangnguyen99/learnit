const express = require('express')
const router = express.Router()
const argon2 = require('argon2')
const jwt = require('jsonwebtoken')
const verifyToken = require('../middlewares/auth')


const User = require('../models/User')

// @route GET api/auth
// @desc Check if user logged in
// @acess Public

router.get('/', verifyToken, async (req, res) => {
  try {
    const user = await User.findById(req.userId).select('-password')
    if (!user) return res.status(400).json({ success: false, message: 'User not found' })

    res.json({ success: true, user })

  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})



// @route POST api/auth/register
// @desc Register user
// @access Public

router.post('/register', async (req, res) => {
  const { username, password, email } = req.body

  // Simple validation
  if (!username || !password || !email)
    return res
      .status(400)
      .json({ success: false, message: 'Missing email or username or password' })
  try {
    // check for existing user
    const user_name = await User.findOne({ username })
    if (user_name)
      return res.status(400).json({ success: false, message: 'Username already exists' })

    const user_email = await User.findOne({ email })
    if (user_email)
      return res.status(400).json({ success: false, message: 'Email already exists' })

    // All good
    const hashedPassword = await argon2.hash(password)
    const newUser = new User({
      email,
      username,
      password: hashedPassword
    })
    await newUser.save()

    // Return token
    const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN_SECRET)

    res.json({ success: true, message: 'User created successfully', accessToken })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})

// @route POST api/auth/login
// @desc Login user
// @access Public

router.post('/login', async (req, res) => {
  const { email, password } = req.body
  // Simple validation
  if (!email || !password)
    return res
      .status(400)
      .json({ success: false, message: 'Missing email and/or password' })

  try {
    // Check for existing user
    const user = await User.findOne({ email })
    if (!user)
      return res.status(400).json({ success: false, message: 'Incorrect email or password' })

    // Username found
    const passwordValid = await argon2.verify(user.password, password)
    if (!passwordValid)
      return res.status(400).json({ success: false, message: 'Incorrect username or password' })

    // All good

    // Return token
    const accessToken = jwt.sign({ userId: user._id }, process.env.ACCESS_TOKEN_SECRET)

    res.json({ success: true, message: 'User logged in successfully', accessToken })
  } catch (error) {
    console.log(error)
    res.status(500).json({ success: false, message: 'Internal server error' })
  }
})

module.exports = router